from sudoku import validate, solve
from flask import Flask, render_template, request

def integer(i):
    try:
        return int(i)
    except ValueError:
        if i == '':
            return 0
        else:
            raise ValueError('Invalid')

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('form.html')

@app.route('/solve', methods=['POST'])
def sudokusolve():
    l = list(map(integer,request.form.values()))
    ll = []
    for i in range(9):
        ll.append(l[i*9:(i+1)*9])
    if not validate(ll):
        return 'Error'
    return render_template('form.html', solution=solve(ll))

if __name__ == '__main__':
    app.run(port=6060)
