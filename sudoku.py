#!/usr/bin/python3
# Sudoku Solver

def column(sudoku):
    return [list(i) for i in zip(*sudoku)]

def block(sudoku):
    l = []
    for a in range(3):
        s = []
        for b in sudoku:
            s+=b[a*3:(a+1)*3]
        l.append(s[0:9])
        l.append(s[9:18])
        l.append(s[18:])
    return l

def blockindex(x,y):
    return (x//3)*3 + (y//3) 

def testbox(sudoku,x,y):
    if sudoku[y][x] != 0:
        return 0, None
    l = []
    l+= sudoku[y]
    l+= column(sudoku)[x]
    l+= block(sudoku)[blockindex(x,y)]
    l = set(l)
    l.remove(0)
    return len(l), l

def flat(sudoku):
    l = []
    for row in sudoku:
        l+=row
    return l

def solve(sudoku):
    for i in range(10000):
        if 0 not in flat(sudoku):
            break
        for x in range(9):
            for y in range(9):
                length, occurences = testbox(sudoku,x,y)
                if length == 8:
                    sudoku[y][x],*_ = set(range(1,10)) - occurences
    return sudoku

def validate(sudoku):
    rows = [*sudoku,*column(sudoku),*block(sudoku)]
    uniqness = all(map( lambda row: (len([i for i in row if i != 0]) == len(set(row)) - 1), rows ))
    occurences = list(filter( lambda l: type(l) == set, [testbox(sudoku,x,y)[1] for x in range(9) for y in range(9)] ))
    solvablity = any([len(set([i for i in row if i != 0])) == 8 for row in occurences])
    return uniqness and solvablity

if __name__ == '__main__':
    sudoku_game = [ 
        [5,3,0,0,7,0,0,0,0],
        [6,0,0,1,9,5,0,0,0],
        [0,9,8,0,0,0,0,6,0],
        [8,0,0,0,6,0,0,0,3],
        [4,0,0,8,0,3,0,0,1],
        [7,0,0,0,2,0,0,0,6],
        [0,6,0,0,0,0,2,8,0],
        [0,0,0,4,1,9,0,0,5],
        [0,0,0,0,8,0,0,7,9]
    ]
    print(*solve(sudoku_game),sep='\n')
